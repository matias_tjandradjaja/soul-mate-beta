// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOULMATEBETA_SoulMateBetaCharacter_generated_h
#error "SoulMateBetaCharacter.generated.h already included, missing '#pragma once' in SoulMateBetaCharacter.h"
#endif
#define SOULMATEBETA_SoulMateBetaCharacter_generated_h

#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_RPC_WRAPPERS
#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesASoulMateBetaCharacter(); \
	friend SOULMATEBETA_API class UClass* Z_Construct_UClass_ASoulMateBetaCharacter(); \
	public: \
	DECLARE_CLASS(ASoulMateBetaCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, SoulMateBeta, NO_API) \
	DECLARE_SERIALIZER(ASoulMateBetaCharacter) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ASoulMateBetaCharacter*>(this); }


#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesASoulMateBetaCharacter(); \
	friend SOULMATEBETA_API class UClass* Z_Construct_UClass_ASoulMateBetaCharacter(); \
	public: \
	DECLARE_CLASS(ASoulMateBetaCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, SoulMateBeta, NO_API) \
	DECLARE_SERIALIZER(ASoulMateBetaCharacter) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ASoulMateBetaCharacter*>(this); }


#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASoulMateBetaCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASoulMateBetaCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoulMateBetaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoulMateBetaCharacter); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ASoulMateBetaCharacter(const ASoulMateBetaCharacter& InCopy); \
public:


#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ASoulMateBetaCharacter(const ASoulMateBetaCharacter& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoulMateBetaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoulMateBetaCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASoulMateBetaCharacter)


#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_8_PROLOG
#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_RPC_WRAPPERS \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_INCLASS \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_INCLASS_NO_PURE_DECLS \
	soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID soul_mate_beta_Source_SoulMateBeta_SoulMateBetaCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
